import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClimateControlComponent } from './climate-control.component';

describe('ClimateControlComponent', () => {
  let component: ClimateControlComponent;
  let fixture: ComponentFixture<ClimateControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClimateControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClimateControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
