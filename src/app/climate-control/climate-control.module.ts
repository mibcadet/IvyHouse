import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClimateControlComponent } from './climate-control.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
      path: '',
      component: ClimateControlComponent
  }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ClimateControlComponent]
})
export class ClimateControlModule { }
