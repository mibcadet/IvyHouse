import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { WeatherComponent } from './weather.component';
import { Routes, RouterModule } from '@angular/router';

import { MaterialModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MediaQueriesModule } from '@angular/flex-layout';

const routes: Routes = [
  {
      path: '',
      component: WeatherComponent
  }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    FlexLayoutModule,
    MediaQueriesModule,
    HttpClientModule,
    RouterModule.forChild(routes)
  ],
  declarations: [WeatherComponent]
})

export class WeatherModule {
}
